<!DOCTYPE html>
<html lang="en">

  <head>
    <title>Prodev</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/creative.min.css" rel="stylesheet">

  </head>
  <body>
    <header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <img class="img-fluid mb-5 d-block mx-auto" src="img/proo.png" alt="">
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p><strong><h2>Welcome To Feasibility</h2></strong></p>
            <a href='{!! url('/list-lab'); !!}' type="submit" class="btnn btn-success fa fa-edit">GO!!</a> 
          </div>
        </div>
      </div>
    </header>
  </body>
</html>