@extends('feasibility.layout')

@section('title','Feasibility|List')

@section('content')

<div class="row" >
  <div class="col-md-14">
    <div class="showback">
      <div class="row">
        <div class="col-md-6"><h4><i class="fa fa-book"></i> Data Pengajuan Feasibility</h4> </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12"> 
    <div class="showback" style="border-radius:3px;">
      <table class="table table-hover table-bordered">
        <thead>
          
          <tr>
            <th rowspan="2" class="text-center">No</th>
            <th rowspan="2" class="text-center">Formula</th>
            <th rowspan="2" class="text-center">Tanggal Masuk</th>
            <th colspan="5" class="text-center">Progress</th>
            <th rowspan="2" class="text-center">Aksi</th>
          </tr>
          
          <tr>
            <th class="text-center">Inputor</th>
            <th class="text-center">Produksi</th>
            <th class="text-center">Kemas</th>
            <th class="text-center">Lab</th>
            <th class="text-center">Finance</th>
          </tr>
        </thead>
        <tbody>
        @foreach($dataF as $dF)
          <tr>
            <td class="text-center">{{ $dF->formula->id }}</td>
            <td class="text-center">{{ $dF->formula->nama_produk }}</td>
            <td class="text-center">{{ $dF->formula->updated_at }}</td>
            <td class="text-center"><a href="/mes/{{$dF->id_feasibility}}" type="submit" class="btn btn-info fa fa-edit"></a>
            <td class="text-center"><a href="/prod/{{$dF->id_feasibility}}" type="submit" class="btn btn-info fa fa-edit"></a>
            <td class="text-center"><a href="/konsep/{{$dF->id_feasibility}}" type="submit" class="btn btn-info fa fa-edit"></a></td>
            <td class="text-center"><a href="/datalab/{{$dF->id_feasibility}}" type="submit" class="btn btn-info fa fa-edit"></a>
            <td class="text-center"><a href="/fin/{{$dF->id_feasibility}}" type="submit" class="btn btn-info fa fa-edit"></a>
            <td class="text-center">
              <div class="btn-group">
                <button class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Selesai" disabled><i class="fa fa-check">Selesai</i></button>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection