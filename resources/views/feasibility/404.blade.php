@extends('feasibility.layout')

@section('title','Feasibility|404')

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-lg-offset-3 p404 centered">
        <img src="img/404.png" alt="">
        <h1>DON'T PANIC!!</h1>
        <h3>There is a data error that you entered.</h3>
        <br>
        <p><a href='{!! url('/list'); !!}' class="btn btn-primary" type="button">Return</a></p>
      </div>
    </div>
  </div>

@endsection