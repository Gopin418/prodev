@extends('inputor.layout')

@section('title','Feasibility|inputor')

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="form-panel">
      <div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a  href="/mes/{{$dataF->id_feasibility}}" type="button" class="btn btn-primary btn-circle" disabled="disabled">1</a>
            <p>Data Mesin</p>
        </div>
        <div class="stepwizard-step">
            <a href="/oh/{{$dataF->id_feasibility}}" type="submit" class="btn btn-default btn-circle">2</a>
            <p>Data Oh</p>
        </div>
        <div class="stepwizard-step">
            <a href="/rateM/{{$dataF->id_feasibility}}" type="button" class="btn btn-default btn-circle">3</a>
            <p>Runtime Mesin</p>
        </div>
        <div class="stepwizard-step">
            <a href="/rateO/{{$dataF->id_feasibility}}" type="button" class="btn btn-default btn-circle">4</a>
            <p>Runtime OH</p>
        </div>
    </div>
</div>
      <div id="DM" class="tab-pane active">
                <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="/mss" method="post">
                  <div class="row">
                    <div class="col-lg-12 detailed">
                      <h4 class="mb">Data aktifitas Mesin</h4>
                      
                        <table class="Table">
                          <thead>
                            <tr>
                              <th></th>
                              <th>workcenter</th>
                              <th>gedung</th>
                              <th class="hidden-phone">kategori</th>
                              <th class="hidden-phone">Activity</th>
                              <th class="hidden-phone">nama kategori</th>
                            </tr>
                          </thead>
                            <div class="col-md-1 col-sm-1 col-xs-12">
                              <input type="hidden" name="finance" maxlength="45" required="required" value="{{$fe->id_feasibility}}" class="form-control col-md-7 col-xs-12">
                            </div>
                            @foreach($mesins as $mesin)
                            <tr>
                              <td><input type="checkbox" id="pmesin" name="pmesin[]" value="{{ $mesin->id_data_mesin }}"></td>
                              <td>{{ $mesin->workcenter }}</td>
                              <td>{{ $mesin->gedung }}</td>
                              <td>{{ $mesin->Direct_Activity }}</td>
                              <td>{{ $mesin->kategori }}</td>
                              <td>{{ $mesin->nama_kategori }}</td>
                            </tr>
                            @endforeach
                        </table>
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">                        
                      <a href='{!! url('/list-mesin'); !!}' class="btn btn-danger" type="button">Cancel</a>
                        <button type="submit" class="btn btn-success">Submit</button>
			                    {{ csrf_field() }}
                      </div>
                    </div>
                  </div>
                </form>
              </div>
      </div>
    </div>
</div>
@endsection