@extends('inputor.layout')

@section('title','Feasibility|inputor')

@section('content')
  
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="form-panel">
      <div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a  href="/mes/{{$dataF->id_feasibility}}" type="button" class="btn btn-primary btn-circle" >1</a>
            <p>Data Mesin</p>
        </div>
        <div class="stepwizard-step">
            <a href="/oh/{{$dataF->id_feasibility}}" type="submit" class="btn btn-default btn-circle" >2</a>
            <p>Data Oh</p>
        </div>
        <div class="stepwizard-step">
            <a href="/rateM/{{$dataF->id_feasibility}}" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
            <p>Runtime Mesin</p>
        </div>
        <div class="stepwizard-step">
            <a href="/rateO/{{$dataF->id_feasibility}}" type="button" class="btn btn-default btn-circle">4</a>
            <p>Runtime OH</p>
        </div>
    </div>
</div>
      <div id="RM" class="tab-pane">
              <div class="row">
                <div class="col-lg-12 detailed">
                  <h4 class="mb">Runtime Mesin</h4>
                    <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center">mesin</th>
                        <th class="text-center">Standar SDM</th>
                        <th class="text-center">Runtime</th>
                        <th class="text-center">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($Mdata as $dM)
                      <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{url('/updatemss')}}/{{$dM->id_mesin}}" method="post">
                      {!!csrf_field()!!}
                      <tr>
                        <td>{{ $dM->meesin->Direct_Activity }}</td>
                        <td class="text-center">10 Orang</td>
                        @if($dM->runtime==NULL)
                        <td><input id="runtime" name="runtime" class="date-picker form-control col-md-7 col-xs-12" type="text" required></td> 
                        <td><button type="submit" class="btn btn-primary">Submit</button></td>
                        @else
                        <td class="text-center">{{$dM->runtime}} Menit</td>
                        <td class="text-center"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal{{ $dM->id_mesin  }}" ">Edit</button>
                  <div class="modal fade" id="exampleModal{{ $dM->id_mesin  }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content text-left ">
                        <div class="modal-header">
                          <h3 class="modal-title" id="exampleModalLabel">Edit Data
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button><h3>
                        </div>
                        <div class="modal-body">
                        <form >
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Runtime Mesin:</label>
                <input id="runtime" value="{{$dM->runtime}}" name="runtime" class="date-picker form-control" type="text">
              </div></div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <a href="{{ route('mesin.destroy', $dM->id_mesin) }}" class="btn btn-danger btn-sm">Hapus</a>
                      </td>
                    @endif
                    </tr>
                      </form>
                      @endforeach
                    </tbody>
                    </table>
                </div>
              </div>
            </div>


@endsection