<!DOCTYPE html>
<html lang="en">

<head>
<title>@yield('title')</title></title>

<link href="{{ URL::asset('img/pro.png') }}" rel="icon">
<link href="{{ URL::asset('img/apple-touch-icon.png') }}" rel="apple-touch-icon">
<link href="{{ URL::asset('lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('lib/advanced-datatable/css/jquery.dataTables.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('lib/advanced-datatable/css/jquery.dataTables.min.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ URL::asset('css/custom.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('lib/advanced-datatable/css/dataTables.bootstrap.css') }}" />
<link href="{{ URL::asset('css/dataTables.bootstrap4.min.css') }}">
<link href="{{ URL::asset('css/dataTables.min.css') }}">
<link href="{{ URL::asset('css/asri.css') }}" rel="stylesheet">
<link href="{{ URL::asset('lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('css/custom.min.css" rel="stylesheet') }}">
<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/style-responsive.css') }}" rel="stylesheet">
<link href="{{ URL::asset('lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />

</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.html" class="logo"><b>Pro<span>dev</span></b></a>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
      </div>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a href="/" class="logout" href="login.html">Logout</a></li>
        </ul>
      </div>
    </header>
    
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
    <div id="sidebar" class="nav-collapse ">
  <ul class="sidebar-menu" id="nav-accordion">
    <p class="centered"><a href="profile.html"><img src="{{ URL::asset('img/proo.png')}}" class="img-circle" width="80"></a></p>
      <h5 class="centered">Feasibility</h5>
        <li>
          <a href="/mes/{{$dataF->id_feasibility}}">
            <i class="fa fa-edit"></i>
              <span>Form </span>
          </a>
        </li>
        <li>
          <a href="/inbox/{{$dataF->id_feasibility}}">
            <i class="fa fa-envelope"></i>
              <span>Mail </span>
         </a>
        </li>
  </ul>
 </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
        <section id="main-content">
      <section class="wrapper site-min-height">
        @yield('content')
      </section>
      <!-- /wrapper -->
    </section>
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
        <strong>PT. NUTRIFOOD INDONESIA</strong>
        </p>
        <a href="blank.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
    </section>
    <script src="{{ URL::asset('lib/jquery/jquery.min.js')}}"></script>
  <script type="text/javascript" language="javascript" src="{{ URL::asset('lib/advanced-datatable/js/jquery.dataTables.js')}}"></script>
  <script type="text/javascript" language="javascript" src="{{ URL::asset('lib/advanced-datatable/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{ URL::asset('lib/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{ URL::asset('lib/jquery-ui-1.9.2.custom.min.js')}}"></script>
  <script src="{{ URL::asset('lib/jquery-ui-1.9.2.custom.min.js')}}"></script>
  <script src="{{ URL::asset('lib/jquery-ui-1.9.2.custom.min.js')}}"></script>
  <script src="{{ URL::asset('lib/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{ URL::asset('lib/jquery-ui-1.9.2.custom.min.js')}}"></script>
  <script src="{{ URL::asset('lib/jquery.ui.touch-punch.min.js')}}"></script>
  <script class="include" type="text/javascript" src="{{ URL::asset('lib/jquery.dcjqaccordion.2.7.js')}}"></script>
  <script src="{{ URL::asset('lib/jquery.scrollTo.min.js')}}"></script>
  <script src="{{ URL::asset('lib/jquery.nicescroll.js')}}" type="text/javascript"></script>
  <script src="{{ URL::asset('lib/common-scripts.js')}}"></script>
	<script src="{{ URL::asset('lib/bootstrap/bootstrap.min.js')}}" type="text/javascript"></script>
  <script src="{{ URL::asset('js/datatables.min.js')}}"</script>
  <script src="{{ URL::asset('lib/dataTables.bootstrap4.min.css')}}"</script>
  <script type="text/javascript">$('.Table').DataTable({
      "language": {
        "search": "Cari :",
        "lengthMenu": "Tampilkan _MENU_ data",
        "zeroRecords": "Tidak ada data",
        "emptyTable": "Tidak ada data",
        "info": "Menampilkan data _START_  - _END_  dari _TOTAL_ data",
        "infoEmpty": "Tidak ada data",
        "paginate": {
          "first": "Awal",
          "last": "Akhir",
          "next": ">",
          "previous": "<"
        }
      }
    });</script>
    <script>
    $('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('New message to ' + recipient)
  modal.find('.modal-body input').val(recipient)
})
</script>
</body>

</html>