@extends('inputor.layout')

@section('title','Feasibility|inputor')

@section('content')
<!-- page content -->
<div class="col-md-12 col-sm-12 col-xs-12 content-panel">
  <div class="x_panel">
    <h1 class="text-center">Form STD Yield Produksi</h1>
      <div class="x_content">
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="/stdd" method="post">
          <div class="form-group">
            <div class="col-md-1 col-sm-1 col-xs-12">
              <input type="hidden" name="finance" maxlength="45" required="required" value="{{$fe->id_feasibility}}" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-1 col-sm-1 col-xs-12">
              <input type="hidden" name="status" maxlength="45" required="required" value="selesai" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
        
				  <div class="form-group">
            <label class="control-label col-md-2 col-sm-5 col-xs-12">Rever Exist</label>
              <div class="col-md-7 col-sm-6 col-xs-12">
                <input type="text" name="exist" maxlength="45" class="form-control" required>
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-3 col-xs-12">Nama Item</label>
              <div class="col-md-7 col-sm-6 col-xs-12">
                <input type="text" name="nama" maxlength="125" class="form-control" required>
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-3 col-xs-12">Yield Baru</label>
              <div class="col-md-7 col-sm-6 col-xs-12">
                <input type="text" name="yield" maxlength="100" class="form-control" required>
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-3 col-xs-12">Box</label>
              <div class="col-md-7 col-sm-6 col-xs-12">
                <input type="text" name="boxx" maxlength="8" class="form-control" required>
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-3 col-xs-12">Acid</label>
              <div class="col-md-7 col-sm-6 col-xs-12">
                <input type="text" name="acid" maxlength="75" class="form-control" required>
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-3 col-xs-12">Lye</label>
              <div class="col-md-7 col-sm-6 col-xs-12">
                <input type="text" name="lye" maxlength="50" class="form-control" required>
              </div>
          </div>
          <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                <a href="/mes/{{$dataF->id_feasibility}}" class="btn btn-danger" type="button">Cancel</a>
                  <button class="btn btn-warning" type="reset">Reset</button>
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2">Submit</button>
                    <div class="modal" id="myModal2">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                          <div class="modal-body">
                            <h4>Yakin Dengan Data Yang Anda Masukan??</h4>
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Submit</button>
			                      {{ csrf_field() }}
                          </div>
                        </div>
                      </div>
                    </div>
              </div>
            </div>
          </div>  
        </form>
      </div>
    </div>
  <div>
<!-- /page content -->
@endsection