@extends('inputor.layout')
@section('title','Feasibility|inputor')
@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="form-panel">
      <div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a  href="/mes/{{$dataF->id_feasibility}}" type="button" class="btn btn-primary btn-circle" >1</a>
            <p>Data Mesin</p>
        </div>
        <div class="stepwizard-step">
            <a href="/oh/{{$dataF->id_feasibility}}" type="submit" class="btn btn-default btn-circle" >2</a>
            <p>Data Oh</p>
        </div>
        <div class="stepwizard-step">
            <a href="/rateM/{{$dataF->id_feasibility}}" type="button" class="btn btn-default btn-circle">3</a>
            <p>Runtime Mesin</p>
        </div>
        <div class="stepwizard-step">
            <a href="/rateO/{{$dataF->id_feasibility}}" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
            <p>Runtime OH</p>
        </div>
    </div>
</div>
<div id="RO" class="tab-pane">
              <div class="row">
                <div class="col-lg-12 detailed">
                  <h4 class="mb">Runtime OH</h4>
                  <table class="table table-hover  table-bordered">
                  <thead>
                    <tr>
                      <th class="text-center">OH</th>
                      <th class="text-center">Standar SDM</th>
                      <th class="text-center">Runtime</th>
                      <th class="text-center">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                    @foreach($dataO as $dO)
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{url('/updateohh')}}/{{$dO->id_oh}}" method="post">
                    {!!csrf_field()!!}
                      <td>{{ $dO->dataoh->direct_activity }}</td>
                      <td class="text-center">10 Orang</td>
                      @if($dO->runtime==NULL)
                      <td><input id="runtime" name="runtime" class="date-picker form-control col-md-7 col-xs-12" type="text" required></td> 
                      <td><button type="submit" class="btn btn-primary">Submit</button></td>
                      @else
                      <td class="text-center">{{$dO->runtime}} Menit</td>
                      <td class="text-center"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal{{ $dO->id_oh  }}" ">Edit</button>
                  <div class="modal fade" id="exampleModal{{ $dO->id_oh  }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content text-left ">
                        <div class="modal-header">
                          <h3 class="modal-title" id="exampleModalLabel">Edit Data
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button><h3>
                        </div>
                        <div class="modal-body">
                        <form >
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Runtime OH:</label>
                <input id="runtime" value="{{ $dO->runtime }}"  name="runtime" class="date-picker form-control" type="text">
              </div></div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <a href="{{ route('mesin.destroy',$dO->id_oh) }}" class="btn btn-danger btn-sm">Hapus</a>
                      </td>
                             @endif
                            </tr>
                            </form>
                          @endforeach
                        </tbody>
                        </table>
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">                        
                      <a href="/std/{{$dataF->id_feasibility}}" class="btn btn-success" type="button">Selesai</a>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
      </div>
    </div>
</div>
@endsection