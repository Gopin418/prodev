@extends('produksi.layout')

@section('title','Feasibility|Produksi')

@section('content')

<!-- page start-->
<div class="row mt">
  <div class="col-sm-12 content-panel">
    <section class="panel">
      <header class="panel-heading wht-bg">
        <h4 class="gen-case">
            Inbox
            <form action="#" class="pull-right mail-src-position">
              <div class="input-append">
                <input type="text" class="form-control " placeholder="Search Mail">
              </div>
            </form>
          </h4>
      </header>
    <div class="panel-body minimal">
      <div class="mail-option">
        <div class="chk-all">
          <div class="pull-left mail-checkbox">
            <input type="checkbox" class="">
          </div>
          <div class="btn-group">
            <a data-toggle="dropdown" href="#" class="btn mini all">All<i class="fa fa-angle-down "></i></a>
              <ul class="dropdown-menu">
                <li><a href="#"> None</a></li>
                <li><a href="#"> Read</a></li>
                <li><a href="#"> Unread</a></li>
              </ul>
          </div>
        </div>
        <div class="btn-group">
          <a data-original-title="Refresh" data-placement="top" data-toggle="dropdown" href="#" class="btn mini tooltips"><i class=" fa fa-refresh"></i></a>
        </div>
        <div class="btn-group hidden-phone">
          <a data-toggle="dropdown" href="#" class="btn mini blue">More<i class="fa fa-angle-down "></i></a>
            <ul class="dropdown-menu">
              <li><a href="#"><i class="fa fa-pencil"></i> Mark as Read</a></li>
              <li><a href="#"><i class="fa fa-ban"></i> Spam</a></li>
              <li class="divider"></li>
              <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
            </ul>
        </div>
        <div class="btn-group">
          <a data-toggle="dropdown" href="#" class="btn mini blue">Move to<i class="fa fa-angle-down "></i></a>
            <ul class="dropdown-menu">
              <li><a href="#"><i class="fa fa-pencil"></i> Mark as Read</a></li>
              <li><a href="#"><i class="fa fa-ban"></i> Spam</a></li>
              <li class="divider"></li>
              <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
            </ul>
        </div>
        </div>
          <ul class="unstyled inbox-pagination">
            <li>
              <a class="np-btn" href="#"><i class="fa fa-angle-left  pagination-left"></i></a>
            </li>
            <li>
              <a class="np-btn" href="#"><i class="fa fa-angle-right pagination-right"></i></a>
            </li>
          </ul>
        </div>
        <div class="table-inbox-wrap ">
          <table class="table table-inbox table-hover">
            <tbody>
              <tr class="unread">
              <tr>
                <td> </td>  
                <td class="text-center">Pengirim</td>
                <td class="text-center">Subject</td>
                <td class="text-center">Message</td>
                <td class="text-center">Waktu</td>
              </tr>
                @foreach($inboxs as $box)
              <tr>
                <td class="inbox-small-cells">
                  <input type="checkbox" class="mail-checkbox">
                </td>  
                <td>{{ $box->pengirim }}</td>
                <td>{{ $box->subject }}</td>
                <td>{{ $box->message }}</td>
                <td>{{ $box->created_at }}</td>
              </tr>
                @endforeach
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div> 
@endsection