@extends('finance.layout')

@section('title','Feasibility|finance')

@section('content')

<div class="col-md-12 col-sm-12 col-xs-12 content-panel">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <h2>Ubah data</h2>
        <div class="x_content">
          <ul class="nav nav-tabs nav-justified">
            <li class="active">
              <a data-toggle="tab" href="#DM">Data mesin</a>
            </li>
            <li>
              <a data-toggle="tab" href="#RM">Runtime mesin</a>
            </li>
            <li>
              <a data-toggle="tab" href="#DO">Data OH</a>
            </li>
            <li>
              <a data-toggle="tab" href="#RO">Runtime OH</a>
            </li>
          </ul>
        </div>
        
        <!-- /panel-heading -->
        <div class="panel-body">
          <div class="tab-content">
            <div id="DM" class="tab-pane active">
              <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="/mss" method="post">
                <div class="row">
                  <div class="col-lg-12 detailed">
                    <h4 class="mb">Data aktifitas Mesin</h4>
                      <table class="table table-bordered" id="sampleTable">
                      <thead>
                        <tr>
                          <th></th>
                          <th>workcenter</th>
                          <th>gedung</th>
                          <th class="hidden-phone">kategori</th>
                          <th class="hidden-phone">Activity</th>
                          <th class="hidden-phone">nama kategori</th>
                        </tr>
                      </thead>
                  <div class="col-md-1 col-sm-1 col-xs-12">
                    <input type="hidden" name="finance" maxlength="45" required="required" value="{{$fe->id_feasibility}}" class="form-control col-md-7 col-xs-12">
                  </div>
                    @foreach($mesins as $mesin)
                    <tr>
                      <td><input type="checkbox" id="pmesin" name="pmesin[]" value="{{ $mesin->id_data_mesin }}"></td>
                      <td>{{ $mesin->workcenter }}</td>
                      <td>{{ $mesin->gedung }}</td>
                      <td>{{ $mesin->Direct_Activity }}</td>
                      <td>{{ $mesin->kategori }}</td>
                      <td>{{ $mesin->nama_kategori }}</td>
                    </tr>
                    @endforeach
                      </table>
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">                        
                    <a href='{!! url('/list-mesin'); !!}' class="btn btn-danger" type="button">Cancel</a>
                      <button type="submit" class="btn btn-success">Submit</button>
			                {{ csrf_field() }}
                  </div>
                </div>
              </form>
            </div>
                  
            <div id="DO" class="tab-pane">
              <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="/ohh" method="post">
                <div class="row">
                  <div class="col-lg-12  detailed">
                    <h4 class="mb">Data aktifitas OH</h4>
                      <table class="table table-bordered" id="Table">
                      <thead>
                        <tr>
                          <th></th>
                          <th>workcenter</th>
                          <th>gedung</th>
                          <th class="hidden-phone">Activity</th>
                          <th class="hidden-phone">kategori</th>
                          <th class="hidden-phone">Driver</th>
                        </tr>
                      </thead>
                    <div class="col-md-1 col-sm-1 col-xs-12">
                      <input type="hidden" name="finance" maxlength="45" required="required" value="{{$fe->id_feasibility}}" class="form-control col-md-7 col-xs-12">
                    </div>
                      @foreach($aktifitas as $ak)
                      <tr>
                        <td><input type="checkbox" id="oh" name="oh[]" value="{{ $ak->id_aktifitasOH }}"></td>
                        <td>{{ $ak->workcenter }}</td>
                        <td>{{ $ak->gedung }}</td>
                        <td>{{ $ak->direct_activity }}</td>
                        <td>{{ $ak->kategori }}</td>
                        <td>{{ $ak->driver }}</td>
                      </tr>
                      @endforeach
                      </table>
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">                        
                      <button type="submit" class="btn btn-success">Submit</button>
			                {{ csrf_field() }}
                    </div>
                  </div>
                </div>
              </form>
            </div>
                    
            <!-- /tab-pane -->
            <div id="RM" class="tab-pane">
              <div class="row">
                <div class="col-lg-12 detailed">
                  <h4 class="mb">Runtime Mesin</h4>
                    <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center">mesin</th>
                        <th class="text-center">Standar SDM</th>
                        <th class="text-center">Runtime</th>
                        <th class="text-center">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($Mdata as $dM)
                      <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{url('/updatemss')}}/{{$dM->id_mesin}}" method="post">
                      {!!csrf_field()!!}
                      <tr>
                        <td>{{ $dM->meesin->Direct_Activity }}</td>
                        <td class="text-center">10 Orang</td>
                        @if($dM->runtime==NULL)
                        <td><input id="runtime" name="runtime" class="date-picker form-control col-md-7 col-xs-12" type="text" required></td> 
                        <td><button type="submit" class="btn btn-primary">Submit</button></td>
                        @else
                        <td class="text-center">{{$dM->runtime}} Menit</td>
                        <td><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal{{ $dM->id_mesin  }}" ">Edit</button>
                  <div class="modal fade" id="exampleModal{{ $dM->id_mesin  }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h3 class="modal-title" id="exampleModalLabel">Edit Data</h3>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <form >
                          <div class="form-group">
                            <label for="recipient-name" class="col-form-label">SDM:</label>
                            <input id="runtime" name="runtime" class="date-picker form-control col-md-7 col-xs-12" type="text">
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <a href="{{ route('mesin.destroy', $dM->id_mesin) }}" class="btn btn-danger btn-sm">Hapus</a>
                      </td>
                    @endif
                    </tr>
                      </form>
                      @endforeach
                    </tbody>
                    </table>
                </div>
              </div>
            </div>

            <!-- /tab-pane -->
            <div id="RO" class="tab-pane">
              <div class="row">
                <div class="col-lg-12 detailed">
                  <h4 class="mb">Runtime OH</h4>
                  <table class="table table-hover  table-bordered">
                  <thead>
                    <tr>
                      <th class="text-center">OH</th>
                      <th class="text-center">Standar SDM</th>
                      <th class="text-center">Runtime</th>
                      <th class="text-center">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                    @foreach($dataO as $dO)
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{url('/updateohh')}}/{{$dO->id_oh}}" method="post">
                    {!!csrf_field()!!}
                      <td>{{ $dO->dataoh->direct_activity }}</td>
                      <td class="text-center">10 Orang</td>
                      @if($dO->runtime==NULL)
                      <td><input id="runtime" name="runtime" class="date-picker form-control col-md-7 col-xs-12" type="text" required></td> 
                      <td><button type="submit" class="btn btn-primary">Submit</button></td>
                      @else
                      <td class="text-center">{{$dO->runtime}} Menit</td>
                      <td><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal2{{ $dO->id_oh  }}" ">Edit</button>
                <div class="modal fade" id="exampleModal2{{ $dO->id_oh }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal- ">
                      <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Edit Data</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form >
                        <div class="form-group">
                          <label for="recipient-name" class="col-form-label">SDM:</label>
                          <input id="runtime" name="runtime" class="date-picker form-control col-md-7 col-xs-12" type="text">
                        </div>
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <a href="{{ route('oh.destroyoh', $dO->id_oh) }}" class="btn btn-danger btn-sm">Hapus</a>
                @endif
                    </tr>
                    </form>
                    @endforeach
                        </tbody>
                      </table>
                      <div class="ln_solid"></div>
  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">    
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2">selesai</button>
                          <div class="modal" id="myModal2">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                  <h4>Yakin Dengan Data tersebut??</h4>
                                </div>
                                <div class="modal-footer">
                                <a href="/ubahsdm/{{$dataF->id_feasibility}}"" class="btn btn-primary" type="button">Ubah Data SDM??</a>
                                <a href="/fin/{{$dataF->id_feasibility}}" class="btn btn-primary" type="submit">selesai</a>
                                </div>
                              </div>
                            </div>
                          </div>                 
                      
                        </div>
                  </div>
                </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div> 
    </div>
  </div>   
</div>

@endsection