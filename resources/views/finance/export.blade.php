@extends('finance.layout')

@section('title','Feasibility|finance')

@section('content')

<!-- page content -->
<div class="col-md-12 col-sm-12 col-xs-12 content-panel">
    <div class="x_panel">
          <h2>Feasibility</h2>
            <div class="x_content">
              <form id="demo-form2"  class="form-horizontal form-label-left" action="/standar" method="post">
                <div class="ln_solid"></div>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                      <a href='{!! url('/kom'); !!}' class="btn btn-primary" type="button">Export data ke PDF</a>
                    </div>
                  </div>
                </div>
              </form>
            </div>
        </div>
      </div>
    </div>
  </div>
<!-- /page content -->

@endsection