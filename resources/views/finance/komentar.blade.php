@extends('finance.layout')

@section('title','Feasibility|finance')

@section('content')

  <div class="panel-body">
    <div class="col-sm-12">
      <section class="panel">
        <header class="panel-heading wht-bg">
          <h4 class="gen-case"> Message</h4>
        </header>
        <div class="panel-body">
          <div class="compose-mail">
            <form role="form-horizontal" method="post" action="/fnn">
              <div class="form-group">
                <label for="to" class="">To:</label>
                  <div class="col-md-10 col-sm-11 col-xs-12">
                    <select class="form-control" name="users">
                      <option>--> <--</option>
                      <option>Inputor</option>
                      <option>Produksi</option>
                    </select>
                  </div>
              </div>
              <div class="col-md-1 col-sm-1 col-xs-12">
              <input type="hidden" name="finance" maxlength="45" required="required" value="{{$fe->id_feasibility}}" class="form-control col-md-7 col-xs-12">
              </div>
              <div class="">
                      <input type="hidden" name="finance" maxlength="45" required="required" value="{{$fe->id_feasibility}}" class="form-control col-md-7 col-xs-12">
                    </div>
              <div class="form-group">
                <label for="subject" class="">Subject:</label>
                  <input type="text" tabindex="1" id="subject" class="form-control" name="sub">
              </div>
              <div class="compose-editor">
              <textarea name="mail" class="form-control" rows="15"></textarea>
              </div>
              <div class="compose-btn">
                <button type="submit" class="btn btn-success">Send</button>
			          {{ csrf_field() }}
              </div>
            </form>
          </div>
        </div>
      </section>
    </div>
  </div>      

@endsection