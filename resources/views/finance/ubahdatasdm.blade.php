@extends('finance.layout')

@section('title','Feasibility|finance')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
	<h2>Data SDM Mesin</h2>
	</div>
	<div class="panel-body">
    <table class="table table-hover table-bordered">
      <thead>
        <tr>
        <th class="text-center">No</th>
          <th class="text-center">mesin</th>
          <th class="text-center">Runtime</th>
          <th class="text-center">standar SDM</th>
          <th class="text-center">SDM</th>
          <th class="text-center">Aksi</th>
        </tr>
      </thead>
      <tbody>
      <?php $no = 0;?>
        @foreach($dataM as $dM)
        
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{url('/updatesdmmesin')}}/{{$dM->id_mesin}}" method="post">
        {!!csrf_field()!!}
    <tr>
    <div class="col-md-1 col-sm-1 col-xs-12">
      <input type="hidden" name="status" maxlength="45" required="required" value="selesai" class="form-control col-md-7 col-xs-12">
    </div>
    <div class="col-md-1 col-sm-1 col-xs-12">
        <input type="hidden" name="finance" maxlength="45" required="required" value="{{$fe->id_feasibility}}" class="form-control col-md-7 col-xs-12">
    </div>
    <?php $no++ ;?>
    <td>{{ $no }}</td>
    <td>{{ $dM->meesin->Direct_Activity }}</td>
    <td class="text-center">{{ $dM->runtime  }} Menit</td>
    <td class="text-center">10 Orang </td>
    @if($dM->SDM==NULL)
    <td><input id="SDM" name="SDM" class="date-picker form-control col-md-7 col-xs-12" type="text"></td> 
    <td><button type="submit" class="btn btn-primary">Submit</button></td>
    @else
    <td class="text-center">{{$dM->SDM}} orang</td>
    <td><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal{{ $dM->id_mesin  }}" ">Edit</button>
    <div class="modal fade" id="exampleModal{{ $dM->id_mesin  }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">Edit Data</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form >
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">SDM:</label>
                <input id="SDM" name="SDM" class="date-picker form-control col-md-7 col-xs-12" type="text">
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endif
    </tr>
        </form>
        @endforeach
      </tbody>
    </table>
  </div>
</div> 

<div class="panel panel-default">
	<div class="panel-heading">
	<h2>Data SDM OH</h2>
	</div>
	<div class="panel-body">
    <table class="table table-hover table-bordered">
    <thead>
      <tr>
      <th class="text-center">No</th>
        <th class="text-center">OH</th>
        <th class="text-center">Runtime</th>
        <th class="text-center">Standar SDM</th>
        <th class="text-center">SDM</th>
        <th class="text-center">Aksi</th>
      </tr>
    </thead>
    <tbody>
    <?php $no = 0;?>
      @foreach($dataO as $dO)
      <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{url('/updatesdmoh')}}/{{$dO->id_oh}}" method="post">
          {!!csrf_field()!!}
          <?php $no++ ;?>
          <tr>
            <td>{{ $no }}</td>
          <td>{{ $dO->dataoh->direct_activity }}</td>
          <td class="text-center">{{ $dO->runtime }} Menit</td>
          <td class="text-center">10 Orang</td>
          @if($dO->SDM==NULL)
          <td><input id="SDM" name="SDM" class="date-picker form-control col-md-7 col-xs-12" type="text"></td> 
          <td><button type="submit" class="btn btn-primary">Submit</button></td>
          @else
          <td class="text-center">{{$dO->SDM}} orang</td>
          <td><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal2{{$dO->id_oh}}" >Edit</button>
          <div class="modal fade" id="exampleModal2{{$dO->id_oh}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Edit Data</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">SDM:</label>
            <input id="SDM" name="SDM" class="date-picker form-control col-md-7 col-xs-12" type="text">
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
          @endif
        </tr>
      </form>
      @endforeach
    </tbody>
    </table>
<div class="ln_solid"></div>
  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">    
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2">selesai</button>
                          <div class="modal" id="myModal2">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                  <h4>Yakin Dengan Data tersebut??</h4>
                                </div>
                                <div class="modal-footer">
                                <a href="/ubah/{{$dataF->id_feasibility}}"" class="btn btn-primary" type="button">Ubah Data Mesin??</a>
                                <a href="/fin/{{$dataF->id_feasibility}}" class="btn btn-primary" type="submit">selesai</a>
                                </div>
                              </div>
                            </div>
                          </div> 
  </div>
</div>
  </div>
</div> 

@endsection