@extends('finance.layout')

@section('title','Feasibility|finance')

@section('content')

<!-- page content -->
<div class="col-md-12 col-sm-12 col-xs-12 content-panel">
    <div class="x_panel">
          <h2>Feasibility</h2>
            <div class="x_content">
              <form id="demo-form2"  class="form-horizontal form-label-left" action="/standar" method="post">

                <div class="ln_solid"></div>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                      <a href="/kom/{{$dataF->id_feasibility}}" class="btn btn-primary" type="button">tidak setuju</a>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2">setuju</button>
                          <div class="modal" id="myModal2">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                  <h4>Yakin Dengan Data tersebut??</h4>
                                </div>
                                <div class="modal-footer">
                                <a href="/ubah/{{$dataF->id_feasibility}}"" class="btn btn-primary" type="button">Ubah Data</a>
                                  <a href='{!! url('/list-finance'); !!}' class="btn btn-primary" type="button">Ya</a>
                                </div>
                              </div>
                            </div>
                          </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
        </div>
      </div>
    </div>
  </div>
<!-- /page content -->

@endsection