@extends('finance.layout')

@section('title','Feasibility|finance')

@section('content')
<div class="col-md-12 col-sm-12 col-xs-12 content-panel">
    <div class="x_panel">
        <h2>Form Data Finance</h2>
          <div class="x_content">
            <ul class="nav nav-tabs nav-justified">
              <li class="active">
                <a data-toggle="tab" href="#SM">Mesin</a>
              </li>
              <li>
                <a data-toggle="tab" href="#SO">OH</a>
              </li>
            </ul>
          </div>
          <!-- /panel-heading -->
          <div class="panel-body">
            <div class="tab-content">
              <div id="SM" class="tab-pane active">
                  <div class="row">
                    <div class="col-lg-12 detailed">
                    <h4 class="mb">Data SDM Mesin</h4>
                    <table class="table table-hover table-bordered">
                        <thead>
                          <tr>
                          <th class="text-center">No</th>
                            <th class="text-center">mesin</th>
                            <th class="text-center">Runtime</th>
                            <th class="text-center">SDM</th>
                            <th class="text-center">Standart SDM</th>
                            <th class="text-center">Rate mesin</th>
                            <th class="text-center">Rate SDM</th>
                            <th class="text-center">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $no = 0;?>
                          @foreach($dataM as $dM)
                          <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{url('/updatefnMesin')}}/{{$dM->id_mesin}}" method="post">
                            {!!csrf_field()!!}
                            <?php $no++ ;?>
                            <tr>
                            <td>{{ $no }}</td>
                              <td>{{ $dM->meesin->Direct_Activity }}</td>
                              <td class="text-center">{{ $dM->runtime }} Menit</td>
                              <td class="text-center">{{ $dM->SDM }} Orang</td>
                              @if($dM->standar_sdm==NULL)
                              <td><input name="standar_sdm" class="form-control col-md-7 col-xs-12" type="text" required></td>     
                              <td><input name="rate_mesin" class="form-control col-md-7 col-xs-12" type="text" required></td> 
                              <td><input name="rate_sdm" class="form-control col-md-7 col-xs-12" type="text" required></td>
                              <td><button type="submit" class="btn btn-primary">Submit</button></td>
                             @else
                             <td class="text-center">{{$dM->standar_sdm}} orang</td>
                             <td class="text-center">{{$dM->rate_mesin}}</td>
                             <td class="text-center">{{$dM->rate_sdm}}</td>
                             <td><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal{{ $dM->id_mesin  }}" ">Edit</button>
    <div class="modal fade" id="exampleModal{{ $dM->id_mesin  }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">Edit Data</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form >
              <div class="form-group">
              <label for="recipient-name" class="col-form-label">standar sdm:</label>
                <input id="standar_sdm" name="standar_sdm" value="{{$dM->standar_sdm}}" class="date-picker form-control col-md-7 col-xs-12" type="text">
                <div>
                <label for="recipient-name" class="col-form-label">rate mesin:</label>
                <input id="rate_mesin" name="rate_mesin" value="{{$dM->rate_mesin}}" class="date-picker form-control col-md-7 col-xs-12" type="text">
                <div>
                <label for="recipient-name" class="col-form-label">rate sdm:</label>
                <input id="rate_sdm" name="rate_sdm" value="{{$dM->rate_sdm}}" class="date-picker form-control col-md-7 col-xs-12" type="text">
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
                             @endif
                            </tr>
                          </form>
                           @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
              </div>
                  
              <div id="SO" class="tab-pane">
                  <div class="row">
                    <div class="col-lg-12  detailed">
                    <h4 class="mb">Data SDM OH</h4>
                    <table class="table table-hover table-bordered">
                        <thead>
                          <tr>
                          <th class="text-center">No</th>
                            <th class="text-center">OH</th>
                            <th class="text-center">Runtime</th>
                            <th class="text-center">SDM</th>
                            <th class="text-center">Standart SDM</th>
                            <th class="text-center">Rate OH</th>
                            <th class="text-center">Rate SDM</th>
                            <th class="text-center">aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $no = 0;?>
                          @foreach($dataO as $dO)
                          <?php $no++ ;?>
                          <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{url('/updatefnOh')}}/{{$dO->id_oh}}" method="post">
                            {!!csrf_field()!!}
                            <tr>
                            <td>{{ $no }}</td>
                              <td>{{ $dO->dataoh->direct_activity }}</td>
                              <td class="text-center">{{ $dO->runtime}} Menit</td>
                              <td class="text-center">{{ $dO->SDM}} Orang</td>

                              @if($dO->standar_sdm==NULL)
                              <td><input name="standar_sdm" class="form-control col-md-7 col-xs-12" type="text" required></td>           
                              <td><input name="rate_mesin" class="form-control col-md-7 col-xs-12" type="text" required></td> 
                              <td><input name="rate_sdm" class="form-control col-md-7 col-xs-12" type="text" required></td>
                              <td><button type="submit" class="btn btn-primary">Submit</button></td>
                             @else
                             <td class="text-center">{{$dO->standar_sdm}} orang</td>
                             <td class="text-center">{{$dO->rate_mesin}}</td>
                             <td class="text-center">{{$dO->rate_sdm}}</td>
                             <td><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal2{{ $dO->id_oh  }}" ">Edit</button>
    <div class="modal fade" id="exampleModal2{{ $dO->id_oh  }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">Edit Data</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          <div class="form-group">
              <label for="recipient-name" class="col-form-label">standar sdm:</label>
                <input id="standar_sdm" name="standar_sdm" value="{{$dO->standar_sdm}}" class="date-picker form-control col-md-7 col-xs-12" type="text">
                <div>
                <label for="recipient-name" class="col-form-label">rate mesin:</label>
                <input id="rate_mesin" name="rate_mesin" value="{{$dO->rate_mesin}}" class="date-picker form-control col-md-7 col-xs-12" type="text">
                <div>
                <label for="recipient-name" class="col-form-label">rate sdm:</label>
                <input id="rate_sdm" name="rate_sdm" value="{{$dO->rate_sdm}}" class="date-picker form-control col-md-7 col-xs-12" type="text">
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
          </div>
        </div>
      </div>
    </div>
                             @endif
                              
                            </tr>
                          </form>
                          @endforeach
                        </tbody>
                      </table>
                      <div class="ln_solid"></div>
                    <div class="form-group">
                      
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">                        
                      <a  href="/akhir/{{$dataF->id_feasibility}}"" class="btn btn-primary" type="button">selesai</a>
                        </div>
                    </div>
                  </div>
              </div>
               
                    </div>
                  </div>
                </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div> 
    </div>
  </div>  
</div> 
@endsection