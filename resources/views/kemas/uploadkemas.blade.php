@extends('kemas.layout')

@section('title','Feasibility|kemas')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Import Data Kemas</h3>
	</div>
	<div class="panel-body">
		<form action="{{ url('/hasil') }}" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}

			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif

			@if (session('error'))
			<div class="alert alert-success">
				{{ session('error') }}
			</div>
			@endif
			<div class="form-group">
				<div>
					<input type="hidden" name="finance" maxlength="45" required="required" value="{{ $id }}" class="form-control col-md-7 col-xs-12">
				</div>
				<label for="">File (.csv)</label>
				<input type="file" class="form-control" name="file">
				<p class="text-danger">{{ $errors->first('file') }}</p>
			</div>
			<div class="form-group">
				<button class="btn btn-primary btn-sm">Upload</button>
			</div>
		</form>

	</div>
</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Hasil Import</h4>
	</div>
	<div class="panel-body">
		<table class="table">
			<tr>
				<th class="text-center">Nama</th>
				<th class="text-center">jumlah kemasan sekunder</th>
				<th class="text-center">jumlah kemasan primer</th>
				<th class="text-center">gramasi</th>
			</tr>
			<tbody>
				<tr>
					<td class="text-center"></td>
					<td class="text-center"></td>
					<td class="text-center"></td>
					<td class="text-center"></td>
				</tr>
			</tbody>
		</table>
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th rowspan="2" class="text-center">No</th>
					<th rowspan="2" class="text-center">kode item</th>
					<th rowspan="2" class="text-center">Description</th>
					<th rowspan="2" class="text-center">Dimensi</th>
					<th rowspan="2" class="text-center">spek</th>
					<th rowspan="2" class="text-center">line mesin</th>
					<th colspan="3" class="text-center">Net Quantity (PPIC)</th>
					<th rowspan="2" class="text-center">unit</th>
					<th colspan="3" class="text-center">Net Quantity (Uom)</th>
					<th rowspan="2" class="text-center">unit</th>
					<th rowspan="2" class="text-center">waste</th>
				</tr>

				<tr>
					<th class="text-center">dus</th>
					<th class="text-center">box</th>
					<th class="text-center">batch</th>

					<th class="text-center">dus</th>
					<th class="text-center">box</th>
					<th class="text-center">batch</th>
				</tr>

			</thead>
			<tbody>
				<?php $no = 0;?>
				@foreach($kemas as $kem)
				<?php $no++ ;?>
				<tr>
					<td>{{ $no }}</td>
					<td class="text-center">{{ $kem->item_code }}</td>
					<td class="text-center">{{ $kem->kode_komputer }}</td>
					<td class="text-center">{{ $kem->dimensi }}</td>
					<td class="text-center">{{ $kem->spek }}</td>
					<td class="text-center">{{ $kem->line_mesin }}</td>
					<td class="text-center">{{ $kem->dus_ppa }}</td>
					<td class="text-center">{{ $kem->box_ppa }}</td>
					<td class="text-center">{{ $kem->batch_ppa }}</td>
					<td class="text-center">{{ $kem->unit_ppa }}</td>
					<td class="text-center">{{ $kem->dus_net }}</td>
					<td class="text-center">{{ $kem->box_net }}</td>
					<td class="text-center">{{ $kem->batch_net }}</td>
					<td class="text-center">{{ $kem->unit_net }}</td>
					<td class="text-center">{{ $kem->waste }}%</td>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>

		<table class="table table-hover table-bordered col-md-6 col-sm-6 col-xs-12">
			<tr>
				<th class="text-right">Disetujui oleh</th>
			</tr>
			<tbody>
				@foreach($kemas as $kem)
				<tr>
					<td class="text-right">{{ $kem->user }}</td>
				</tr>

				@endforeach
				<tr>
					<td class="text-right">R&D Packaging Manager</td>
				</tr>
			</tbody>
		</table>
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
			<a href="/list-kemas" class="btn btn-primary" type="submit">selesai</a>
		</div>
	</div>
</div>

</div>
@endsection
