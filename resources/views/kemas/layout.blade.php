<!DOCTYPE html>
<html lang="en">

<head>
  <title>@yield('title')</title></title>

  <!-- Favicons -->
  <link href="img/pro.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="css/custom.min.css" rel="stylesheet">
  <link href="css/daterangepicker.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="{{ URL::asset('css/custom.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/daterangepicker.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('lib/font-awesome/css/font-awesome.css') }}"> 
  <link rel="stylesheet" href="{{ URL::asset('css/custom.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('lib/bootstrap/css/bootstrap.min.css') }}">
  <link rel="apple-touch-icon" href="{{ URL::asset('img/apple-touch-icon.png') }}">
  <link rel="icon" href="{{ URL::asset('img/pro.png') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/style-responsive.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
</head>

<body> 
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        </div>
      </div>
      <!--logo start-->
      <a href="index.html" class="logo"><img src="{{ URL::asset('img/pro.png') }}"><b>PRO<span>DEV</span></b></a>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
      </div>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
        <li><a href='{!! url('/'); !!}' class="logout btn btn-primary" type="button">Logout</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
      <section class="wrapper site-min-height">
       @yield('content')
    </section>
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
        <strong>PT. NUTRIFOOD INDONESIA</strong>
        </p>
        <a href="blank.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{{ URL::asset('lib/jquery/jquery.min.js') }}"></script>
  <script src="{{ URL::asset('lib/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{ URL::asset('lib/jquery-ui-1.9.2.custom.min.js') }}"></script>
  <script src="{{ URL::asset('lib/jquery.ui.touch-punch.min.js') }}"></script>
  <script src="{{ URL::asset('js/daterangepicker.js') }}"></script>
  <script class="include" type="text/javascript" src="{{ URL::asset('lib/jquery.dcjqaccordion.2.7.js') }}"></script>
  <script src="{{ URL::asset('lib/jquery.scrollTo.min.js') }}"></script>
  <script src="{{ URL::asset('lib/jquery.nicescroll.js') }}" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="{{ URL::asset('lib/common-scripts.js') }}">
  <!-- //////////////////////////////////////////// -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="lib/jquery.ui.touch-punch.min.js"></script>
  <script src="js/daterangepicker.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>

</body>
</html>