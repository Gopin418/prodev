@extends('kemas.layout')

@section('title','Feasibility|kemas')

@section('content')

<!-- page content -->
<div class="col-md-12 col-sm-12 col-xs-12 content-panel">
    <div class="x_panel">
      <h2>Form Konsep Kemas</h2>
        <div class="x_content">
          <div class="x_content">
            <form id="demo-form2"  class="form-horizontal form-label-left" action="/kemass" method="post">
              <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Konsep Kemas</label>
                  <div class="col-md-5 col-sm-5 col-xs-12">
                    <select class="form-control" name="konsepkemas" required>
                      <option>--> <--</option>
                      <option>Tradisional</option>
                      <option>Modern</option>
                    </select>
                  </div>
                    <div class="">
                      <input type="hidden" name="finance" maxlength="45" required="required" value="{{$fe->id_feasibility}}" class="form-control col-md-7 col-xs-12">
                    </div>
            <div class="col-md-1 col-sm-1 col-xs-12">
              <input type="hidden" name="status" maxlength="45" required="required" value="selesai" class="form-control col-md-7 col-xs-12">
            </div>
                    <div class="col-md-1 col-sm-1 col-xs-12">
                      <input name="d" class="date-picker form-control col-md-1 col-sm-2 col-xs-12" type="text" required>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" name="primer">Dus</label>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-12">
                      <input name="s" class="date-picker form-control col-md-7 col-xs-12" type="text" required>
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" name="sekunder">Saset</label>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-12">
                      <input name="g" class="date-picker form-control col-md-7 col-xs-12" type="text" required>
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" name="tersier">Gram</label>
                    </div>
                  </div>

                  <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                        <a href='{!! url('/list-kemas'); !!}' class="btn btn-danger" type="submit">Cancel</a>
						              <button class="btn btn-success" type="reset">Reset</button>
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2">Submit</button>
                            <div class="modal" id="myModal2">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  </div>
                                  <div class="modal-body">
                                    <h4>Yakin Dengan Data Yang Anda Masukan??</h4>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="submit" class="btn btn-success">Submit</button>
			                              {{ csrf_field() }}
                                  </div>
                                </div>
                              </div>
                            </div>
                      </div>
                    </div>
                  </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- /page content -->
@endsection