@extends('labb.layout')

@section('title','Feasibility|Lab')

@section('content')
<!-- page content -->
  <div class="col-md-12 col-sm-12 col-xs-12 content-panel">
    <div class="panel panel-default">
	<div class="panel-heading">
	<h2>* Form Lab</h2>
	</div>
	<div class="panel-body">
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="/lab" method="post">
          <div class="form-group">
            <div class="col-md-1 col-sm-1 col-xs-12">
              <input type="hidden" name="finance" maxlength="45" required="required" value="{{$fe->id_feasibility}}" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
				  <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Refer Exist</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="refer" maxlength="45" required class="form-control col-md-7 col-xs-12">
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Item</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="item" maxlength="100" name="last-name" required class="form-control col-md-7 col-xs-12">
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Jumlah sample</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="sempel" maxlength="8" name="last-name" required class="form-control col-md-7 col-xs-12">
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Parameter</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="parameter" maxlength="8" name="last-name" required class="form-control col-md-7 col-xs-12">
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Total</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="total" maxlength="8" name="last-name" required class="form-control col-md-7 col-xs-12">
              </div>
          </div>
          <div class="form-group">
            <div class="col-md-1 col-sm-1 col-xs-12">
              <input type="hidden" name="status" maxlength="45" required="required" value="selesai" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                <a href='{!! url('/list-lab'); !!}' class="btn btn-danger" type="submit">Cancel</a>
                <button class="btn btn-warning" type="reset">Reset</button>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2">Submit</button>
                  <!-- The Modal -->
                  <div class="modal" id="myModal2">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                          <h4>Yakin Dengan Data Yang Anda Masukan??</h4>
                        </div>
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-success">Submit</button>
			                    {{ csrf_field() }}
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
        </form>
      </div>
    </div>
  <div>
<!-- /page content -->
@endsection