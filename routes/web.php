<?php

//kemas
Route::get('/konsep/{id_feasibility}','kemass\KonsepController@konsep');
Route::post('/kemass','Kemass\KonsepController@store');
Route::get('/list-kemas','fin\FinanceController@masuk');
Route::post('/fin','kemass\KemasController@store');

//layout
Route::get('/','Controller@awal');
Route::get('/list-finance','fin\FinanceController@masuk');
Route::get('/404','Controller@error');
Route::get('list','Controller@list');

//inputor
Route::get('/mes/{id_feasibility}','mesinn\MesinController@create')->name('mes');
Route::get('/oh/{id_feasibility}','mesinn\MesinController@createoh')->name('oh');
Route::get('/rateM/{id_feasibility}','mesinn\MesinController@createrateM')->name('rateM');
Route::get('/rateO/{id_feasibility}','mesinn\MesinController@createrateO')->name('rateO');
Route::post('/mss', 'mesinn\MesinController@Mdata');
Route::get('/hapus/{id_feasibility}', 'mesinn\MesinController@delete');
Route::post('/updatemss/{id_mesin}', 'mesinn\MesinController@runM')->name('updatemss');
Route::post('/ohh', 'mesinn\MesinController@dataO');
Route::post('/updateohh/{id_oh}', 'mesinn\MesinController@runO')->name('updateohh');
Route::post('/stdd', 'mesinn\MesinController@store');
Route::get('/inbox/{id_feasibility}','mesinn\MesinController@inbox');
Route::post('/ps', 'mesinn\MesinController@kirim');
Route::get('/std/{id_feasibility}','mesinn\MesinController@showinsertstd');
Route::get('/list-mesin','fin\FinanceController@masuk');
Route::get('deleteoh/{id}', 'mesinn\MesinController@destroyoh')->name('oh.destroyoh');
Route::get('deletedata/{id}', 'mesinn\MesinController@destroy')->name('mesin.destroy');

//lab
Route::get('/datalab/{id_feasibility}', 'llab\LabController@create');
Route::post('/lab', 'llab\LabController@store');
Route::get('/list-lab','fin\FinanceController@masuk');

//produksi
Route::get('/prod/{id_feasibility}','sdmm\SdmController@create');
Route::post('/km', 'sdmm\SdmController@store');
Route::post('/updatesdmoh/{id_oh}', 'sdmm\SdmController@sdmO')->name('updatesdmoh');
Route::post('/updatesdmmesin/{id_mesin}', 'sdmm\SdmController@sdmM')->name('updatesdmmesin');
Route::get('/pesan/{id_feasibility}', 'sdmm\SdmController@pesan');
Route::get('/list-sdm','fin\FinanceController@masuk');

//finance
Route::get('ubah/{id_feasibility}','fin\FinanceController@ubah');
Route::get('ubahsdm/{id_feasibility}','fin\FinanceController@ubahsdm'); 
Route::get('/fin/{id_feasibility}','fin\FinanceController@create');
Route::get('/kom/{id_feasibility}','fin\FinanceController@komen');
Route::post('/fnn', 'fin\FinanceController@store');
Route::post('/updatefnMesin/{id_mesin}', 'fin\FinanceController@fnM')->name('updatefnMesin');
Route::post('/updatefnOh/{id_oh}', 'fin\FinanceController@fnO')->name('updatefnOh');
Route::get('/akhir/{id_feasibility}','fin\FinanceController@akhir');
Route::get('/inboxfn/{id_feasibility}','fin\FinanceController@inboxfn');
Route::get('/export/{id_feasibility}','fin\FinanceController@export');
Route::post('/standar', 'fin\FinanceController@da');
Route::post('/users','fin\FinanceController@user');

//csv
Route::get('import/{id_feasibility}', 'kemass\KemasController@up');
Route::post('/hasil', 'kemass\KemasController@storeData');
Route::post('/da', 'kemass\KemasController@fn')->name('da');


//Route::get('import/{id_feasibility}',  'kemass\KemasController@up');
//Route::post('import', 'kemass\KemasController@parseImport')->name('import');
