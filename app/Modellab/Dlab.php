<?php

namespace App\Modellab;

use Illuminate\Database\Eloquent\Model;

class Dlab extends Model
{
    protected $table ='fs_biaya_lab';
    protected $primaryKey ='id_lab';

    public function id()
    {
        return $this->hasOne('App\Modelfn\finance','id_feasibility','id_lab');
    }

    public function datalab()
    {
        return $this->belongsTo('App\Modelfn\finance','id_feasibility','id_lab');
    }
}
