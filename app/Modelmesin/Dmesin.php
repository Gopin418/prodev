<?php

namespace App\Modelmesin;


use Illuminate\Database\Eloquent\Model;

class Dmesin extends Model
{
    
    protected $table ='fs_mesin';
    protected $primaryKey='id_mesin';
 
    protected $fillable =[
    'datamesin','runtime','SDM','id_chat'
    ];

    public function meesin()
    {
        return $this->belongsTo('App\Modelmesin\datamesin','id_data_mesin','id_data_mesin');
    }

    public function datasdm()
    {
        return $this->hasMany('App\Modelsdm\sdm','id_SDM','id_mesin');
    }

    public function Dmesin()
    {
        return $this->hasOne('App\Modelfn\finance','id_feasibility','id_mesin');
    }

    public function chat()
    {
        return $this->hasOne('App\Modelfn\pesan');
    }
}
