<?php

namespace App\Modelmesin;

use Illuminate\Database\Eloquent\Model;

class std extends Model
{
    protected $table ='fs_std_yield_produksi';
    protected $primaryKey ='id_SYP';
    public function id()
    {
        return $this->hasOne('App\Modelfn\finance','id_feasibility','id_SYP');
    }
}
