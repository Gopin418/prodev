<?php

namespace App\Modelmesin;

use Illuminate\Database\Eloquent\Model;

class aktifitasOH extends Model
{
    protected $table ='fs_aktifitas_oh';

    public function dataoh()
    {
        return $this->hasMany('App\Modelmesin\oh','id_oh','id_aktifitasOH');
    }
}
