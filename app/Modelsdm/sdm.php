<?php

namespace App\Modelsdm;

use Illuminate\Database\Eloquent\Model;

class sdm extends Model
{
    protected $table ='fs_sdm';

    protected $fillable =[
        'Dmesin','Doh','SDM_mesin','SDM_oh','id_chat'
        ];

    public function sdmmesin()
    {
        return $this->belongsTo('App\Modelmesin\Dmesin','id_SDM','id_mesin');
    }

    public function sdmoh()
    {
        return $this->belongsToMany('App\Modeloh\oh','id_SDM','id_oh');
    }

    public function pesan()
    {
        return $this->hasOne('App\Modelfn\pesan','id_sdm','id_chat');
    }

    public function id()
    {
        return $this->hasOne('App\Modelfn\finance','id_SDM','id_sdm');
    }
}
