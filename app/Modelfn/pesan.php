<?php

namespace App\Modelfn;

use Illuminate\Database\Eloquent\Model;

class pesan extends Model
{
    protected $table ='fs_chat';

    public function chat()
    {
        return $this->hasOne('App\Modelfn\finance','id_feasibility','id_chat');
    }

    public function chatproduksi()
    {
        return $this->hasOne('App\Modelsdm\sdm','id_sdm','id_chat');
    }

    public function chatinputor()
    {
        return $this->hasOne('App\Modelmesin\oh','id_sdm','id_chat');
    }

}
