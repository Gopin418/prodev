<?php

namespace App\Modelfn;

use Illuminate\Database\Eloquent\Model;

class finance extends Model
{
    protected $table ='fs_list';
    protected $primaryKey='id_list';
    protected $incementing='false';
    
    public function kemas()
    {
        return $this->hasOne('App\Modelkemas\userkemas','id_feasibility','id_list');
    }

    
}
