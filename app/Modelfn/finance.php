<?php

namespace App\Modelfn;

use Illuminate\Database\Eloquent\Model;

class finance extends Model
{
    protected $table ='fs_feasibility';
    protected $primaryKey='id_feasibility';
    protected $incementing='false';
    
    public function kemas()
    {
        return $this->hasOne('App\Modelkemas\userkemas','id_feasibility','id_fk');
    }

    public function formula()
    {
        return $this->belongsTo('App\dev\Formula','id_feasibility','id');
    }

    public function mesin()
    {
        return $this->hasMany('App\Modelmesin\Dmesin','id_feasibility','id_mesin');
    }

    public function oh()
    {
        return $this->hasMany('App\Modelmesin\oh','id_feasibility','id_oh');
    }

    public function std()
    {
        return $this->hasOne('App\Modelmesin\std','id_feasibility','id_SYP');
    }

    public function sdm()
    {
        return $this->hasMany('App\Modelsdm\sdm','id_feasibility','id_sdm');
    }

    public function lab()
    {
        return $this->hasOne('App\Modellab\Dlab','id_feasibility','id_lab');
    }

    public function idlab()
    {
        return $this->hasOne('App\Modellab\Dlab','id_feasibility','id_lab');
    }

    public function chat()
    {
        return $this->hasOne('App\Modellab\Dlab','id_feasibility','id_chat');
    }
}
