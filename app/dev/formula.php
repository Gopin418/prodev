<?php

namespace App\dev;

use Illuminate\Database\Eloquent\Model;

class Formula extends Model
{
    protected $table ='formulas';

    public function fs()
    {
        return $this->hasOne('App\ModelFn\finance','id_feasibility','id');
    }
}