<?php

namespace App\Modelkemas;

use Illuminate\Database\Eloquent\Model;

class konsep extends Model
{
    protected $table ='fs_konsep_kemas';
    protected $primaryKey ='id_konsepkemas';
    
    public function konsep()
    {
        return $this->hasOne('App\Modelfn\finance','id_fk','id_konsepkemas');
    }
}
