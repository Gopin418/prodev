<?php

namespace App\Http\Controllers\fin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Modelfn\finance;
use App\Modelfn\pesan;
use App\Modelmesin\oh;
use App\Modelfn\userpesan;
use App\Modelmesin\Dmesin;
use App\Modelmesin\datamesin;
use App\Modelmesin\std;
use App\Modelmesin\aktifitasOH;


class FinanceController extends Controller
{

    public function komen($id_feasibility)
    {
        $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
        $fe=finance::find($id_feasibility);
        return view('finance.komentar',['fe'=>$fe])
        ->with(['dataF' => $dataF]);
    }

    public function akhir($id_feasibility)
    {
        $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
       return view('finance.dataakhir')
       ->with(['dataF' => $dataF]);
    }

    public function export($id_feasibility)
    {
        $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
       return view('finance.export')
       ->with(['dataF' => $dataF]);
    }

    public function inboxfn($id_feasibility)
    {
        $inboxs = pesan::all()->sortByDesc('created_at');
        $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
       return view('finance.inboxfin')
       ->with(['inboxs' => $inboxs])
       ->with(['dataF' => $dataF]);
    }

    public function create($id_feasibility)
    {
        $dataM = Dmesin::with('meesin')->get()->where('id_feasibility', $id_feasibility);
        $dataO = oh::with('dataoh')->get()->where('id_feasibility', $id_feasibility);
        $fe=finance::find($id_feasibility);
        $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
        return view('finance.finance',['fe'=>$fe])
        ->with(['dataM' => $dataM ])
        ->with(['dataO' => $dataO])
        ->with(['dataF' => $dataF]);
    }

    public function store(Request $request)
    {
        $fnn= new pesan;
        $fnn->id_feasibility=$request->finance;
        $fnn->pengirim='finance';
        $fnn->user=$request->get('users');
        $fnn->subject=$request->sub;
        $fnn->message=$request->get('mail');
        $fnn->save();

    	return redirect('/list-finance');
    }

    public function da(Request $request)
    {
        $standar= new finance;
        $standar->standar_rate=$request->srate;
        $standar->standar_sdm=$request->ssdm;

        $standar->save();

    	return redirect('/akhir');
    }

    public function users()
    {
        $users = userpesan::all();
        return view ('finance.komentar')->with(compact('users'));
    }

    public function masuk(){
        $dataF = finance::with('formula')->get();
        return view('feasibility.list')->with(['dataF' => $dataF]);
    }

    public function fnM(Request $request, $id_mesin)
    {
        $data_mesin = Dmesin::where('id_mesin', $id_mesin)->first();
        $input = $request->all();
        $data_mesin->standar_sdm = $input['standar_sdm'];
        $data_mesin->rate_mesin = $input['rate_mesin'];
        $data_mesin->rate_sdm = $input['rate_sdm'];
        $data_mesin->save();

        return redirect()->back();
	 }
	 
	 public function fnO(Request $request, $id_oh)
    {
        $data_oh = oh::where('id_oh', $id_oh)->first();
        $input = $request->all();
        $data_oh->standar_sdm = $input['standar_sdm'];
        $data_oh->rate_mesin = $input['rate_mesin'];
        $data_oh->rate_sdm = $input['rate_sdm'];
        $data_oh->save();

        return redirect()->back();
   }

   public function ubah($id_feasibility)
   {
       $mesins = datamesin::all();
       $aktifitas = aktifitasOH::all();
       $update =Dmesin::all();
       $fe=finance::find($id_feasibility);
       $Mdata = Dmesin::with('meesin')->get()->where('id_feasibility', $id_feasibility);
       $public =Dmesin::all();
       $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
       $dataO = oh::with('dataoh')->get()->where('id_feasibility', $id_feasibility);
       return view('finance.ubahdata',['fe'=>$fe])
       ->with(['update' => $update])
       ->with(['mesins' => $mesins])
       ->with(['dataF' => $dataF])
       ->with(['public' => $public])
       ->with(['aktifitas' => $aktifitas])
       ->with(['Mdata' => $Mdata ])
       // ->with(['datasih' => $datasih ])
       ->with(['dataO' => $dataO]);
   }

   public function ubahsdm($id_feasibility)
    {
        $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
        $dataM = Dmesin::with('meesin')->get()->where('id_feasibility', $id_feasibility);
        $dataO = oh::with('dataoh')->get()->where('id_feasibility', $id_feasibility);
        $fe=finance::find($id_feasibility);
        return view('finance.ubahdatasdm',['fe'=>$fe])
        ->with(['dataM' => $dataM ])
        ->with(['dataO' => $dataO])
        ->with(['dataF' => $dataF]);
    }
}