<?php

namespace App\Http\Controllers\sdmm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Modelfn\pesan;
use App\Modelmesin\oh;
use App\Modelmesin\Dmesin;
use App\Modelfn\finance;

class SdmController extends Controller
{

    public function Ksdm()
    {
       return view('produksi.Ksdm');
    }

    public function pesan($id_feasibility)
    {
        $inboxs = pesan::all()->sortByDesc('created_at')->where('user','produksi');
        $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
        return view('produksi.inboxsdm')
        ->with(['inboxs' => $inboxs])
        ->with(['dataF' => $dataF]);
    }

    public function store(Request $request)
    {
        $km= new pesan;
        $km->pengirim='produksi';
        $km->user='finance';
        $km->subject=$request->sub;
        $km->message=$request->get('mail');

        $km->save();

    	return redirect('/prod');
        
    }

    public function create($id_feasibility)
    {
        $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
        $dataM = Dmesin::with('meesin')->get()->where('id_feasibility', $id_feasibility);
        $dataO = oh::with('dataoh')->get()->where('id_feasibility', $id_feasibility);
        $fe=finance::find($id_feasibility);
        return view('produksi.sdm',['fe'=>$fe])
        ->with(['dataM' => $dataM ])
        ->with(['dataO' => $dataO])
        ->with(['dataF' => $dataF]);
    }

    public function sdmO(Request $request, $id_oh)
    {
        $data_oh = oh::where('id_oh', $id_oh)->first();
        $input = $request->all();
        $data_oh->SDM = $input['SDM'];
        $data_oh->save();

        return redirect()->back();
   }

   public function sdmM(Request $request, $id_mesin)
    {
        $data_mesin = Dmesin::where('id_mesin', $id_mesin)->first();
        $input = $request->all();
        $data_mesin->SDM = $input['SDM'];
        $data_mesin->save();

        return redirect()->back();
   }

}
