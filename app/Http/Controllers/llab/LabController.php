<?php

namespace App\Http\Controllers\llab;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Modellab\Dlab;
use App\Modelfn\finance;

class LabController extends Controller
{

    public function showinsertlab()
    {
        return view('labb/datalab');
    }

    public function listlab()
    {
       return view('feasibility.list');
    }

    public function store(Request $request)
    {
        $lab= new Dlab;
        $lab->id_feasibility=$request->get('finance');
    	$lab->refer_exist=$request->refer;
    	$lab->nama_item=$request->item;

    	$lab->jumlah_sample=$request->sempel;
    	$lab->parameter=$request->parameter;
        $lab->total=$request->total;
        $lab->status=$request->status;

    	$lab->save();

    	return redirect('/list-lab');
    }

    public function create($id_feasibility)
    {
        $fe=finance::find($id_feasibility);
        return view('labb.datalab',['fe'=>$fe]);
    }

}
