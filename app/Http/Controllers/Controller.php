<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use yajra\Datatables\Datatables;
use App\Modelmesin\datamesin;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function awal()
    {
       return view('feasibility/awal');
    }

    public function profil()
    {
       return view('feasibility/profile');
    }

    public function error()
    {
       return view('feasibility.404');
    }

}
