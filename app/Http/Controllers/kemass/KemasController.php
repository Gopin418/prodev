<?php

namespace App\Http\Controllers\kemass;

use Illuminate\Http\Request;
use App\Http\Requests\CsvImportRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Excel;
use App\Imports\KemasImport;
use App\Modelkemas\userkemas;
use App\Modelkemas\CsvData;
use App\Modelfn\finance;

class KemasController extends Controller
{

    public function up(Request $request, $id_feasibility)
    {
      $request->session()->get('id_feasibility');
      $request->session()->put('id_feasibility', $id_feasibility);
      $id = $request->session()->get('id_feasibility');
      $fe=finance::find($id_feasibility);
			$kemas =userkemas::where('id_feasibility', $id_feasibility)->get();
      $dataF = finance::with('formula')->where('id_feasibility', $id_feasibility)->first();
      // dd($id);
        return view('kemas.uploadkemas',['fe'=>$fe], compact('toImport'))
				->with(['dataF' => $dataF])
				->with(['kemas' => $kemas])
        ->with(['id' => $id]);
    }

		public function storeData(Request $request)
    {
      $id = $request->session()->get('id_feasibility');

        //VALIDASI
        $this->validate($request, [
            'file' => 'required|mimes:csv,txt'

        ]);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $data = new userkemas;
            $data->id_feasibility=$request->finance;
            // dd($data);
             //GET FILE
            Excel::import(new KemasImport, $file, $data);
            $lastkemas = DB::table('fs_formula_kemas')->max('id_fk');
            $changekemas = userkemas::where('id_feasibility', '0')->update(['id_feasibility'=>$id]);
            // $changekemas->save();


            return redirect()->back()->with(['success' => 'Upload success']);
        }
        return redirect()->back()->with(['error' => 'Please choose file before']);
    }
}
