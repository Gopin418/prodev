<?php

namespace App\Http\Controllers\kemass;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Imports\UsersImport;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Modelfn\finance;
use App\Modelkemas\konsep;
use App\Modelkemas\userkemas;
use App\Modelkemas\CsvData;
use Excel;

class KonsepController extends Controller
{
  public function konsep($id_feasibility)
    {
        $fe=finance::find($id_feasibility);
        $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
        return view('kemas.konsep',['fe'=>$fe])
        ->with(['dataF' => $dataF]);
    }

    public function store(Request $request)
    {
      $kemass= new konsep;
      $kemass->konsep=$request->get('konsepkemas');
      $kemass->id_feasibility=$request->finance;
      $kemass->s_primer='D';
      $kemass->primer=$request->d;
      $kemass->s_sekunder='S';
      $kemass->sekunder=$request->s;
      $kemass->s_tersier='G';
      $kemass->tersier=$request->g;
      $kemass->status=$request->status;
      $kemass->save();
  
      return redirect('/import/'.$kemass->id_feasibility);
          
    }
  
}