<?php

namespace App\Http\Controllers\mesinn;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use yajra\Datatables\Datatables;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App\Modelmesin\datamesin;
use App\Modelmesin\std;
use App\Modelmesin\aktifitasOH;
use App\Modelmesin\oh;
use App\Modelmesin\Dmesin;
use App\Modelfn\pesan;
use App\Modelfn\finance;
use Redirect;
use Validator;

class MesinController extends Controller
{

    public function showinsertstd($id_feasibility)
    {
        $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
        $fe=finance::find($id_feasibility);
        return view('inputor/std',['fe'=>$fe])
        ->with(['dataF' => $dataF]);
    }

    public function inbox($id_feasibility)
    {
        $inboxs = pesan::all()->sortByDesc('created_at')->where('user','inputor');
        $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
        return view('inputor.inboxmesin')
        ->with(['dataF' => $dataF])
        ->with(['inboxs' => $inboxs]);
    }

    public function createoh($id_feasibility)
    {
        $aktifitas = aktifitasOH::all();
        $fe=finance::find($id_feasibility);
        $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
        $dataO = oh::with('dataoh')->get()->where('id_feasibility', $id_feasibility);
        return view('inputor.oh',['fe'=>$fe])
        ->with(['dataF' => $dataF])
        ->with(['aktifitas' => $aktifitas])
        ->with(['dataO' => $dataO]);
    }

    public function createrateM($id_feasibility)
    {
        $mesins = datamesin::all();
        $update =Dmesin::all();
        $fe=finance::find($id_feasibility);
        $Mdata = Dmesin::with('meesin')->get()->where('id_feasibility', $id_feasibility);
        $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
        return view('inputor.ratemesin',['fe'=>$fe])
        ->with(['update' => $update])
        ->with(['mesins' => $mesins])
        ->with(['dataF' => $dataF])
        ->with(['Mdata' => $Mdata ]);
    }

    public function createrateO($id_feasibility)
    {
        $aktifitas = aktifitasOH::all();
        $fe=finance::find($id_feasibility);
        $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
        $dataO = oh::with('dataoh')->get()->where('id_feasibility', $id_feasibility);
        return view('inputor.rateoh',['fe'=>$fe])
        ->with(['dataF' => $dataF])
        ->with(['aktifitas' => $aktifitas])
        ->with(['dataO' => $dataO]);
    }

    public function create($id_feasibility)
    {
        $mesins = datamesin::all();
        $update =Dmesin::all();
        $fe=finance::find($id_feasibility);
        $Mdata = Dmesin::with('meesin')->get()->where('id_feasibility', $id_feasibility);
        $dataF = finance::with('formula')->get()->where('id_feasibility', $id_feasibility)->first();
        return view('inputor.mesin',['fe'=>$fe])
        ->with(['update' => $update])
        ->with(['mesins' => $mesins])
        ->with(['dataF' => $dataF])
        ->with(['Mdata' => $Mdata ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'exist'=>'required|numeric',
            'nama' => 'required|alpha',
            'yield' => 'required|alpha',
            'boxx'=>'required|numeric',
            'acid' =>'required|numeric',
            'lye' => 'required|numeric',
        ]);


      $stdd= new std;
      $stdd->id_feasibility=$request->finance;
      $stdd->refer_exist=$request->exist;
    	$stdd->nama_item=$request->nama;
      $stdd->yield_baru=$request->yield;
      $stdd->box=$request->boxx;
      $stdd->acid=$request->acid;
        $stdd->lye=$request->lye;
        $stdd->status=$request->status;
      $stdd->save();
        
        return redirect('/list-mesin');
    }

    public function Mdata(Request $request)
    {

        $ms= new Dmesin;
        foreach ($request->input("pmesin") as $pmesin){
            $add_mesin = new Dmesin;
            $add_mesin->id_feasibility=$request->finance;
            $add_mesin->id_data_mesin= $pmesin;
            $add_mesin->save();
    }
    	return redirect::back();
    }

    public function dataO(Request $request)
    {
        
        $aktifitas= new oh;
        foreach ($request->input("oh") as $oh){
            $add_oh = new oh;
            $add_oh->id_feasibility=$request->finance;
            $add_oh->id_aktifitasOH= $oh;
            $add_oh->save();
    }
    	return redirect::back();
    }

    public function runM(Request $request, $id_mesin)
    {
        $data_mesin = Dmesin::where('id_mesin', $id_mesin)->first();
        $input = $request->all();
        $data_mesin->runtime = $input['runtime'];
        $data_mesin->save();

        return redirect()->back();
	 }
	 
	 public function runO(Request $request, $id_oh)
    {
        $data_oh = oh::where('id_oh', $id_oh)->first();
        $input = $request->all();
        $data_oh->runtime = $input['runtime'];
        $data_oh->save();

        return redirect::back();
   }

   public function destroy($id)
    {
        $mesin = Dmesin::find($id)->first();
        $mesin->delete();
        return redirect::back()->with('message', 'Data berhasil dihapus!');
    }

    public function destroyoh($id)
    {
        $mesin = oh::find($id)->first();
        $mesin->delete();
        return redirect::back()->with('message', 'Data berhasil dihapus!');
    }

}