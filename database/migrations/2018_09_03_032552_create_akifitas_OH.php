<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAkifitasOH extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_aktifitas_OH', function (Blueprint $purple) {
            $purple->increments('id_aktifitasOH');
            $purple->enum('workcenter', ['maklon', 'gabungan', 'ciawi', 'sentul', 'cibitung']);
            $purple->string('gedung', 50);
            $purple->string('direct_activity', 225);
            $purple->string('kategori', 65);
            $purple->string('driver', 150);
            $purple->double('harga_OH');
            $purple->integer('defaultSDM');
            $purple->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fs_aktifitas_OH');        
    }
}
