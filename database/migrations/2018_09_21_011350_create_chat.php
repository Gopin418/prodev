<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_chat', function (Blueprint $dua) {
            $dua->increments('id_chat');
            $dua->integer('id_feasibility')->unsigned();
            $dua->string('pengirim');
            $dua->enum('user',['inputor','produksi']);
            $dua->string('subject',225);
            $dua->text('message')->nullable();;
            $dua->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fs_chat');
    }
}
