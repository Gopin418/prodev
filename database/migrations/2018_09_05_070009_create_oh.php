<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        {
            Schema::create('fs_OH', function (Blueprint $blue) {
                $blue->increments('id_oh');
                $blue->integer('id_feasibility')->unsigned();
                $blue->integer('runtime')->nullable();
                $blue->integer('SDM')->nullable();
				$blue->integer('rate_mesin')->nullable();
				$blue->integer('rate_sdm')->nullable();
            	$blue->integer('standar_sdm')->nullable();
                $blue->integer('id_aktifitasOH')->nullable();
                $blue->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fs_OH');
    }
}
