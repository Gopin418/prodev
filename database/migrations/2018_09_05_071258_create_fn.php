<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_feasibility', function (Blueprint $grey) {
            $grey->increments('id_feasibility');
            $grey->integer('id_formula')->unsigned();
            $grey->enum('status_mesin',['selesai','belum selesai','sending'])->nullable();
            $grey->enum('status_sdm',['selesai','belum selesai','sending'])->nullable();
            $grey->enum('status_kemas',['selesai','belum selesai','sending'])->nullable();
            $grey->enum('status_lab',['selesai','belum selesai','sending'])->nullable();
            $grey->text('messange')->nullable();;
            $grey->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fs_feasibility');
    }
}
