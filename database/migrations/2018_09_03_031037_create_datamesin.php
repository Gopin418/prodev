<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatamesin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_datamesin', function (Blueprint $satu) {
            $satu->increments('id_data_mesin');
            $satu->enum('workcenter', ['ciawi', 'sentul', 'cibitung']);
            $satu->enum('gedung', ['CIAWI_GD D','CIAWI_GD A','CIAWI_GD G','CIAWI_GD H','CIBITUNG_PROD DAIRY','CIBITUNG_PROD NS','SENTUL_PROD SENTUL']);
            $satu->enum('kategori',['mixing', 'filling', 'packing']);
            $satu->string('Direct_Activity', 225);
            $satu->string('nama_kategori',150);
            $satu->double('rate_mesin');
            $satu->integer('defaultSDM');
            $satu->double('harga_SDM');
            $satu->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fs_datamesin');
    }
}
