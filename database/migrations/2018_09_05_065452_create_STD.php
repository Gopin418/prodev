<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSTD extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_STD_yield_produksi', function (Blueprint $red) {
            $red->increments('id_SYP');
            $red->integer('id_feasibility')->unsigned();
            $red->string('refer_exist', 50);
            $red->string('nama_item', 150);
            $red->string('yield_baru', 100);
            $red->string('box',5);
            $red->string('acid', 25);
            $red->string('lye', 25);
            $red->enum('status',['selesai','belum selesai'])->default('belum selesai');
            $red->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fs_STD_yield_produksi');
    }
}
