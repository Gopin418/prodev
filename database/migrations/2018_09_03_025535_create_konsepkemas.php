<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKonsepkemas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_konsep_kemas', function (Blueprint $yellow) {
            $yellow->increments('id_konsepkemas');
            $yellow->integer('id_feasibility')->unsigned();
            $yellow->enum('konsep',['Modern','Tradisional']);
            $yellow->integer('primer');
            $yellow->string('s_primer',6);
            $yellow->integer('sekunder');
            $yellow->string('s_sekunder',6);
            $yellow->integer('tersier');
            $yellow->string('s_tersier',6);
            $yellow->enum('status',['selesai','belum selesai'])->default('belum selesai');
            $yellow->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fs_konsep_kemas');
    }
}
