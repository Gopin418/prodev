<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        {
            Schema::create('fs_list', function (Blueprint $table) {
                $table->increments('id_list');
                $table->integer('id_feasibility')->unsigned();
                $table->enum('kemas',['selesai','belum selesai']);
                $table->enum('inputor',['selesai','belum selesai']);
                $table->enum('produksi',['selesai','belum selesai']);
                $table->enum('lab',['selesai','belum selesai']);
                $table->enum('feasibility',['selesai','belum selesai']);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fs_list');
    }
}
