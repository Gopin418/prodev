<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSDM extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_SDM', function (Blueprint $dua) {
            $dua->increments('id_SDM');
            $dua->integer('id_feasibility')->unsigned();
            $dua->integer('id_mesin')->unsigned();
            $dua->integer('id_oh')->unsigned();
            $dua->integer('SDM_mesin');
            $dua->integer('SDM_oh');
            $dua->integer('id_chat')->nullable();
            $dua->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
