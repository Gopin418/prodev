<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePremix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premixs', function (Blueprint $table) {
          $table->integer('fortail_id')->index();
          $table->string('utuh');
          $table->double('koma');
          $table->string('utuh_cpb');
          $table->double('koma_cpb');
          $table->increments('id');
          $table->text('keterangan')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('premixs');
    }
}
