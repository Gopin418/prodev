<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_mesin', function (Blueprint $white) {
            $white->increments('id_mesin');
            $white->integer('id_feasibility')->unsigned();
            $white->integer('id_data_mesin')->nullable();
            $white->integer('runtime')->nullable();
            $white->integer('SDM')->nullable();
            $white->integer('rate_mesin')->nullable();
            $white->integer('rate_sdm')->nullable();
            $white->integer('standar_sdm')->nullable();
            $white->integer('id_chat')->nullable();
            $white->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fs_mesin');
    }
}
