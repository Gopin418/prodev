<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_biaya_lab', function (Blueprint $grey) {
            $grey->increments('id_lab');
            $grey->integer('id_feasibility')->unsigned();
            $grey->string('refer_exist', 50);
            $grey->string('nama_item', 100);
            $grey->integer('jumlah_sample');
            $grey->integer('parameter');
            $grey->double('total');
            $grey->enum('status',['selesai','belum selesai'])->default('belum selesai');
            $grey->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fs_biaya_lab');
    }
}
